export class ApiConstant {
    BASE_URL = 'http://localhost:5000/'
    LOGIN = 'user'
    REGISTER = 'register'
    KANBAN = 'kanban'
    UPDATE_KANBAN = 'updatekanban'
}
